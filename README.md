# Assessment

![](images/architecture.png)

* Create [terraform](https://gitlab.com/ramiroduarteavalos/tools/terraform) modules

* Install [direnv](https://direnv.net/) to ~/elenas-${ENVIRONMENT}
```bash
mkdir ~/elenas-dev
echo "export KUBECONFIG=~/.kube/eks-elenas-dev" ~/.kube/eks-elenas-dev
chmod 400 ~/.kube/eks-elenas-dev
direnv allow
```
```bash
kubectl get nodes
NAME                            STATUS   ROLES    AGE   VERSION
ip-172-22-12-114.ec2.internal   Ready    <none>   50m   v1.20.4-eks-6b7464
ip-172-22-24-145.ec2.internal   Ready    <none>   50m   v1.20.4-eks-6b7464
ip-172-22-35-41.ec2.internal    Ready    <none>   50m   v1.20.4-eks-6b7464
```
* Create namespaces
```bash
kubectl create ns gitlab
kubectl create ns dev
kubectl create ns traefik
```
* Install [gitlab-runner](https://docs.gitlab.com/charts/charts/gitlab/gitlab-runner)
## Install gitlab-runner to k8s with helm chart
### Copy token of gitlab-runner

![](images/gitlab-token.png)

```bash
kubectl create ns gitlab
kubectl create secret generic -n gitlab gitlab-runner --from-literal=runner-registration-token='xxxxxxxxxxxxxxxxxxxx' --from-literal=runner-token=''
helm add repo gitlab https://charts.gitlab.io 
helm upgrade --install gitlab gitlab/gitlab-runner -n gitlab -f ../env/gitlab-runner/values.dev.yaml --debug
```
* Install [traefik](https://github.com/traefik/traefik-helm-chart)
## create an API Token

![](images/cloudflare-token.png)

```bash
kubectl create secret generic -n traefik traefik-secrets --from-literal=CF_API_EMAIL='XXXXXXXXXXXXX' --from-literal=CF_API_KEY='XXXXXXXXXXXXX'
helm repo add traefik https://helm.traefik.io/traefik
helm upgrade traefik --install traefik/traefik --values ../env/traefik/values.dev.yaml -n traefik --debug
```
* Create [ingress](https://gitlab.com/ramiroduarteavalos/tools/ingress)
```bash
helm upgrade dev --install ../ingress/ -f ../env/values.dev.yaml --debug
```

* Create [repo helm](https://github.com/hypnoglow/helm-s3)

![](images/create-repo-chart.png)

```bash
helm s3 init s3://elenas.chart/
helm repo add elenas s3://elenas.chart/
helm repo list
NAME                	URL                                               
elenas           	    s3://elenas.chart
```
* Execute pipeline [frontend](https://gitlab.com/ramiroduarteavalos/frontend/) && [backend](https://gitlab.com/ramiroduarteavalos/backend/)


| frontend | [url](https://dev.cloude.ar/) |

![](images/react.png)

| backend | [url](https://backend.dev.cloude.ar/welcome/) |

![](images/python.png)

## 🛠️

* [terraform](https://registry.terraform.io/modules/terraform-aws-modules/eks/aws/latest) 
* [Amazon Elastic Kubernetes Service](https://aws.amazon.com/es/eks/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc&eks-blogs.sort-by=item.additionalFields.createdDate&eks-blogs.sort-order=desc)
* [helm](https://helm.sh/)

## Authors
| Name | Position |
| ------ | ------ |
| Ramiro Duarte Avalos | DevOps Engineer / SRE |
